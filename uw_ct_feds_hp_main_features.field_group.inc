<?php
/**
 * @file
 * uw_ct_feds_hp_main_features.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_hp_main_features_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feature_text_item|node|feds_homepage_main_feature|default';
  $field_group->group_name = 'group_feature_text_item';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_main_feature';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'feature text',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'title_field',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'feature text',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-feature-text-item field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_feature_text_item|node|feds_homepage_main_feature|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_feature_text_item|node|feds_homepage_main_feature|full';
  $field_group->group_name = 'group_feature_text_item';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_homepage_main_feature';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'feature text',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'title_field',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'feature text',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-feature-text-item field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_feature_text_item|node|feds_homepage_main_feature|full'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('feature text');

  return $field_groups;
}
