<?php

/**
 * @file
 * uw_ct_feds_hp_main_features.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_hp_main_features_features_override_default_overrides() {
  // This code is only used for UI in features. Export alter hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission.
  $overrides["user_permission.administer rh_node.roles|anonymous user"] = 'anonymous user';
  $overrides["user_permission.administer rh_node.roles|authenticated user"] = 'authenticated user';

  return $overrides;
}
