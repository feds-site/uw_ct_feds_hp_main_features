<?php
/**
 * @file
 * uw_ct_feds_hp_main_features.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_hp_main_features_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_homepage_main_feature';
  $context->description = 'Feds homepage main feature view';
  $context->tag = 'Feds';
  $context->conditions = array();
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_main_feature-block' => array(
          'module' => 'views',
          'delta' => 'feds_main_feature-block',
          'region' => 'feds_features',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Feds homepage main feature view');
  $export['feds_homepage_main_feature'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_homepage_main_features';
  $context->description = 'Displays main feature items block on a site\'s front page.';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_main_feature-block' => array(
          'module' => 'views',
          'delta' => 'feds_main_feature-block',
          'region' => 'feds_features',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays main feature items block on a site\'s front page.');
  t('Feds');
  $export['feds_homepage_main_features'] = $context;

  return $export;
}
