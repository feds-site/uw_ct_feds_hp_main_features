<?php
/**
 * @file
 * uw_ct_feds_hp_main_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_hp_main_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access site in maintenance mode'.
  $permissions['access site in maintenance mode'] = array(
    'name' => 'access site in maintenance mode',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: 'create feds_homepage_main_feature content'.
  $permissions['create feds_homepage_main_feature content'] = array(
    'name' => 'create feds_homepage_main_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_homepage_main_feature content'.
  $permissions['delete any feds_homepage_main_feature content'] = array(
    'name' => 'delete any feds_homepage_main_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_homepage_main_feature content'.
  $permissions['delete own feds_homepage_main_feature content'] = array(
    'name' => 'delete own feds_homepage_main_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_homepage_main_feature content'.
  $permissions['edit any feds_homepage_main_feature content'] = array(
    'name' => 'edit any feds_homepage_main_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_homepage_main_feature content'.
  $permissions['edit own feds_homepage_main_feature content'] = array(
    'name' => 'edit own feds_homepage_main_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_homepage_main_feature revision log entry'.
  $permissions['enter feds_homepage_main_feature revision log entry'] = array(
    'name' => 'enter feds_homepage_main_feature revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_main_feature authored by option'.
  $permissions['override feds_homepage_main_feature authored by option'] = array(
    'name' => 'override feds_homepage_main_feature authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_main_feature authored on option'.
  $permissions['override feds_homepage_main_feature authored on option'] = array(
    'name' => 'override feds_homepage_main_feature authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_main_feature promote to front page option'.
  $permissions['override feds_homepage_main_feature promote to front page option'] = array(
    'name' => 'override feds_homepage_main_feature promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_main_feature published option'.
  $permissions['override feds_homepage_main_feature published option'] = array(
    'name' => 'override feds_homepage_main_feature published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_main_feature revision option'.
  $permissions['override feds_homepage_main_feature revision option'] = array(
    'name' => 'override feds_homepage_main_feature revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_main_feature sticky option'.
  $permissions['override feds_homepage_main_feature sticky option'] = array(
    'name' => 'override feds_homepage_main_feature sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_homepage_main_feature content'.
  $permissions['search feds_homepage_main_feature content'] = array(
    'name' => 'search feds_homepage_main_feature content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
